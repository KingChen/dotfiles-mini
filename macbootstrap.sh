# install theme
# install zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
chsh -s $(which zsh)
# ln -s ~/.dracula-theme/zsh/dracula.zsh-theme ~/.oh-my-zsh/themes/dracula.zsh-theme

curl -fLo ~/.oh-my-zsh/themes/shades-of-purple.zsh-theme --create-dirs \
    https://raw.githubusercontent.com/ahmadawais/shades-of-purple-iterm2/master/shades-of-purple.zsh-theme


# soft link dotfiles
sh ./linkdotfiles.sh

# vscode sync: 2642091db0b766ae62106467f9b7207aefb1b9a1
